# ----------------------------------------------------------------------r
# decafast.py
#
# Decaf abstract syntax node definitions.
# ----------------------------------------------------------------------
'''
Class  Structure for nodes

Node
  ClassDecl
  InterfaceDecl
  FunctionDecl
  PrototypeDecl
  Variable
  Type
  Statement
    AssignStmt
    IfStmt
    WhileStmt
    ForStmt
    ReturnStmt
    PrintStmt
    BreakStmt
    NewStmt
    NewArrayStmt
  Expression
    NameExp
    Num
    Bool
    ArithmeticExpr
    ConditionalExpr
    EqualityExpr
    CallExpr
'''
import llvm
from llvm import core as llvm_core
class Node(object):
    '''
    An ast consists of nodes. This class represents that node object.
    All the nodes are child of this class.
    '''

    #static variable to maintain unique ids for all nodes.
    node_count = 0
    def __init__(self):
    	Node.node_count += 1
    	self.node_id = Node.node_count

    def __eq__(self, other):
        try:
            return self.__dict__ == other.__dict__
        except AttributeError:
            return False

    def codegen(self, bundle):
        '''
            Does all code generation related operations for this node.
            Could have been implemented using the visitor pattern but
            that gets a lil awkward in python

            The bundle is supposed to have all the necessary objects
            needed for compilation. Like the module, builder and scope
            objects.
        '''
        #raise Exception("Codegeneration not implemented in %s" % (str(self),))
        print "Codegen not implemented for %s" % (type(self),)

    def __str__(self):
        return("%s : %s"%(self.node_id,type(self)))

    def accept(self, visitor):
        """Accept method for visitor classes (see cvisitor.py)."""
        
        return self._accept(self.__class__, visitor)
        
    def _accept(self, klass, visitor):
        """Accept implementation.  This is actually a recursive
        function that dynamically figures out which visitor method to
        call.  This is done by appending the class' name to 'visit_', so if
        the node class is called MyNode, then this method tries
        calling visitor.visit_MyNode().  If that node doesn't exist, then
        it recursively attempts to call the visitor method
        corresponding to the class' superclass (e.g.,
        visitor.visit_Node())."""
        
        visitor_method = getattr(visitor, "visit_%s" % klass.__name__, None)
        if visitor_method == None:
            bases = klass.__bases__
            last = None
            for i in bases:
                last = self._accept(i, visitor)
            return last
        else:
            return visitor_method(self)


class ClassDecl(Node):
    '''
    This class representas a class in Decaf. Class has a name of type ID 
    and list of methods of type MethodDecls.
    '''
    def __init__(self, n, extends=None, implements=[], body=None):
        super(ClassDecl,self).__init__()
        self.name = n
        self.body = body
        self.extends = extends
        self.implements = implements

    def __str__(self):
        return "ClassDecl(id=%s,name=%s, \nextends=%s, implements=%s)" % \
        (self.node_id, self.name, self.extends, self.implements)

#    def accept(self, visitor):
#        if visitor.visit_ClassDecl(self):
#            for decl in self.body:
#                decl.accept(visitor)

class InterfaceDecl(Node):
    '''
    This class representas an interface in Decaf. Class has a name of type ID 
    and list of methods of type MethodDecls.
    '''
    def __init__(self, n, body=None):
        super(InterfaceDecl,self).__init__()
        self.name = n
        self.body = body
	self.type = 'interf_decl'

    def __str__(self):
        return "InterfaceDecl(id=%s, \nname=%s)" % (self.node_id, self.name)
    
#    def accept(self, visitor):
#        if visitor.visit_InterfaceDecl(self):
#            for decl in self.body:
#                decl.accept(visitor)

class FunctionDecl(Node):
    '''
    This class represents a method declaration. 
    A method in Decaf has 
    ret_type: Return Type
    name: name of the function
    params: Parameters
    body: Body
    '''
    def __init__(self, tn, n, params, b):
        super(FunctionDecl,self).__init__()
        self.ret_type = tn
        self.name = n 
        self.params = params
        self.body = b
        self.type = 'fun_decl'

    def has_return(self):
        for stmts in self.body:
            if isinstance(stmts, ReturnStmt):
                return 1
        return 0


    def __str__(self):
        return "FunctionDecl(id=%s, \nret_type=%s, \nname=%s, \nparams=%s)" % \
            (self.node_id, self.ret_type, self.name, self.params)

#    def accept(self, visitor):
#        if visitor.visit_FunctionDecl(self):
#            for decl in self.body:
#                decl.accept(visitor)

    def _get_llvm_type(self,tp):
        '''
            Helper method to get the type associated with a typename
            use for return type and params of the function
        '''
        if tp == "void":
            return llvm_core.Type.void()
        elif isinstance(tp, Type):
            return tp.codegen()
        else:
            raise ValueError("Handling %s for types not implemented"%(type(tp),))


    def codegen(self, bundle):
        # Make sure that the name has not been defined before in the scope. Decaf doesnt allow names to be redefined
        if bundle.scope.has_name(self.name.value):
            raise Exception("Redefinition of the name %s"%(self.name.value,))
        # Todo : make sure no two arguments of the same function have thesame name
        # get and set the type for the function return type and params
        ret_type = self._get_llvm_type(self.ret_type)
        params = [p.type.codegen() for p in self.params if isinstance(p,Node)]
        # Create the llvm function in the module
        self.func_type = llvm_core.Type.function(ret_type,params,False)
        llvm_func = llvm_core.Function.new(bundle.module,self.func_type, self.name.value)
        # make sure that it wasnt defined before. If it was, then raise 
        # error. All functions are supposed to be defined just once.
        if not llvm_func.name == self.name.value:
            llvm_func.delete()
            raise Exception("Redefinition of function %s" % (self.name.value,))
        # Add fn to the current scope, push a scope, add argument names to the scope. 
        self.llvm_func = llvm_func
        bundle.scope.set(self.name.value, self.llvm_func)
        bundle.scope.push_scope()
       #create the body for the function
        self.entry_blk = self.llvm_func.append_basic_block("entry")
        old_builder = bundle.builder
        #print "old_builder, ", str(old_builder)
        #if not old_builder is None:
        #    print "bb, ",str(old_builder.basic_block)
        bundle.builder = llvm_core.Builder.new(self.entry_blk)

        # Handle the function arguments, making them mutable
        arg_names = [p.name.value for p in self.params if isinstance(p,Node)]
        for arg, arg_name in zip(self.llvm_func.args,arg_names):
            arg.name = arg_name
            allocated = bundle.builder.alloca(arg.type,arg_name)
            bundle.builder.store(arg, allocated)
            bundle.scope.set(arg_name,allocated)
            
        # try to codegen the body. If doesnt succeed, raise error and delete the function
        try:
            for item in self.body:
                item.codegen(bundle)
        except:
            self.llvm_func.delete()
            raise
        #print "bb, " ,str(bundle.builder.basic_block)

        # if this is a void function, return void
        if self.ret_type == "void":
            bundle.builder.ret_void()

        bundle.scope.pop_scope()
        bundle.builder = old_builder
        self.llvm_func.verify()

        return self.llvm_func
        

class PrototypeDecl(Node):
    '''
    This class represents a Prototype declaration. 
    A Prototype in Decaf has 
    retType: Return Type
    name: name of the function
    params: Parameters
    '''
    def __init__(self, tn, n, params):
        super(PrototypeDecl,self).__init__()
        self.ret_type = tn
        self.name = n 
        self.params = params
	self.type = 'func_prototype'

    def __str__(self):
        return "PrototypeDecl(id=%s, \ntype=%s, \nname=%s, \nparams=%s)" % \
            (self.node_id, self.ret_type, self.name, self.params)

#    def accept(self, visitor):
#        if visitor.visit_PrototypeDecl(self):
#            for decl in self.body:
#                decl.accept(visitor)

class Type(Node):
    '''
    This class Represents a Variable Type.
    t: Type of the variable
    dim: dimension of the variable like int[], int[]
    '''
    def __init__(self, t, size):
        super(Type,self).__init__()
        self.type = t               # t would be 'INT', 'FLOAT', 'BOOL', 'DOUBLE', 'STRING'
        self.size = size

    def is_array(self):
        return (self.size > 0)

    def __str__(self):
        return "Type(id=%s, \ntype=%s)" % \
            (self.node_id, self.type)

    def codegen(self):
        '''
            just return the appropriate llvm type for this type.
        '''
        if self.type == "int":
            el_type = llvm_core.Type.int() 
        elif self.type == "bool":
            el_type = llvm_core.Type.int(1)
        elif self.type == "double":
            el_type = llvm_core.Type.float()
        elif self.type == "string":
            el_type = llvm_core.Type.pointer(llvm_core.Type.int(8))
        else:
            raise Exception("Not implemented types for type %s" % (self.type))
        if not self.is_array():
            return el_type
        else:
            return llvm_core.Type.array(el_type,self.size)


class VariableDecl(Node):
    '''
    This class Represents a Variable.
    t: Type of the variable
    n: name of the variable
    dim: dimension of the variable like int[][], int[]
    '''
    def __init__(self, t, n):
        super(VariableDecl,self).__init__()
        n.type = t
        self.name = n
        self.type = t

    def __str__(self):
        return "VariableDecl(id=%s, \nname=%s, \ntype=%s)" % \
            (self.node_id, self.name, self.type)

    def codegen(self,bundle):
        ptr = bundle.builder.alloca(self.type.codegen(), self.name.value)
        bundle.scope.set(self.name.value,ptr)
        return ptr

class Statement(Node):
    '''
    '''
 #   def accept(self, visitor):
 #       visitor.visit_Statement(self)


class AssignmentStmt(Statement):
    '''
    This class represents an assignment statement.
    left: LHS 
    rhs: RHS
    '''
    def __init__(self, lhs, rhs):
        super(AssignmentStmt,self).__init__()
        self.lhs = lhs
        self.rhs = rhs

    def __str__(self):
        return "AssignmentStmt(id=%s, \nlhs=%s, \nrhs=%s)" %\
            (self.node_id, self.lhs, self.rhs)

#    def accept(self, visitor):
#        if visitor.visit_IfStmt(self):
#            for decl in self.lhs:
#                decl.accept(visitor)
#            for decl in self.rhs:
#                decl.accept(visitor)

    def codegen(self,bundle):
        right = self.rhs.codegen(bundle)
        left = bundle.scope.get(self.lhs.value.strip())
        #print "l_stor",type(left.type)
        #print left.type
        #print "r_stor",type(right)
        #print right.type
        return bundle.builder.store(right,left)

class IfStmt(Statement):
    '''
    Class representing if statement.
    cond: Condition for entering if.
    stmts: statements inside if.
    else_stmts: statements inside else_statement
    '''
    def __init__(self, cond, stmts, else_stmts):
        super(IfStmt,self).__init__()
        self.condition = cond 
        self.statements = stmts 
        self.else_stmts = else_stmts

    def has_return(self):
        for stmts in self.statements:
            if isinstance(stmts, ReturnStmt):
                return True
        return False

    def else_has_return(self):
        for stmts in self.else_stmts:
            if isinstance(stmts, ReturnStmt):
                return True
        return False

    def __str__(self):
        return "IfStmt(id=%s, \ncondition=%s, \nstatements=%s, \nelse_stmts=%s)" %\
            (self.node_id, self.condition, self.statements, self.else_stmts)

    def codegen(self,bundle):
        #get the current basic block and the function to which this block belongs
        old_blk = bundle.builder.basic_block
        bundle.scope.push_scope()
        function = old_blk.function
        # create the basic blocks for if, then and else
        # we could have done away with the need for the if block. But having an extra if block will simply make the IR more readable.
        if_blk = function.append_basic_block("if_condition")
        then_blk = function.append_basic_block("if_then")
        else_blk = function.append_basic_block("if_else")
        after_if = None
        
        #explicitly branch to the if block
        bundle.builder.branch(if_blk)

        # populate the if block first.
        bundle.builder.position_at_end(if_blk)
        res = self.condition.codegen(bundle)
        # do a conditional branch
        bundle.builder.cbranch(res, then_blk, else_blk)
        
        # Now the then block
        bundle.builder.position_at_end(then_blk)
        #print "if then stmts : ",self.statements
        import collections
        if_blk_terminated = False
        if isinstance(self.statements, collections.Iterable):
            for s in self.statements:
                if isinstance(s, ReturnStmt) or isinstance(s, BreakStmt):
                    if_blk_terminated = True
                s.codegen(bundle)
        else:
            if isinstance(self.statements, ReturnStmt) or isinstance(self.statements, BreakStmt):
                if_blk_terminated = True
            self.statements.codegen(bundle)
        # branch over to the after_if block at the end of then
        if not if_blk_terminated:
            if after_if is None:
                after_if = function.append_basic_block("after_if")
            bundle.builder.branch(after_if)

        # Now the else block
        else_blk_terminated = False
        bundle.builder.position_at_end(else_blk)
        if isinstance(self.else_stmts, collections.Iterable):
            for s in self.else_stmts:
                if isinstance(s, ReturnStmt) or isinstance(s, BreakStmt):
                    else_blk_terminated = True
                s.codegen(bundle)
        else:
            self.else_stmts.codegen(bundle)
            if isinstance(self.else_stmts, ReturnStmt) or isinstance(self.else_stmts, BreakStmt):
                else_blk_terminated = True

        if not else_blk_terminated:
            if after_if is None:
                after_if = function.append_basic_block("after_if")
            bundle.builder.branch(after_if) 

        if not after_if is None:
            bundle.builder.position_at_end(after_if)

        bundle.scope.pop_scope()


#    def accept(self, visitor):
#        if visitor.visit_IfStmt(self):
#            for decl in self.stmts:
#                decl.accept(visitor)
#            for decl in self.else_stmts:
#                decl.accept(visitor)

class WhileStmt(Statement):
    '''
    Class representing while statement.
    cond: Condition for entering if.
    stmts: statements inside if.
    '''
    def __init__(self, cond, stmts):
        super(WhileStmt, self).__init__()
        self.condition = cond 
        self.statements = stmts 

    def __str__(self):
        return "WhileStmt(id=%s, \ncondition=%s, \nstatements=%s)" %\
            (self.node_id, self.condition, self.statements)

    def has_return_stmt(self):
        for stmts in self.statements:
            if isinstance(stmts, ReturnStmt):
                return True


#    def accept(self, visitor):
#        if visitor.visit_WhileStmt(self):
#            for decl in self.stmts:
#                decl.accept(visitor)

    def codegen(self, bundle):
        #Create the loop and after_loop block
        function = bundle.builder.basic_block.function
        pre_loop = bundle.builder.basic_block
        pre_loop = function.append_basic_block("while_pre_loop")
        # Add an extra layer to the scope.
        bundle.scope.push_scope()

        # explicitly branch to the loop block
        bundle.builder.branch(pre_loop)
        # Position the builder at the beginning of the loop block
        bundle.builder.position_at_end(pre_loop)
        # loop condition and the conditional branch
        res = self.condition.codegen(bundle)
        loop_blk = function.append_basic_block("while_loop")
        after_loop = function.append_basic_block("while_after_loop")
        bundle.builder.cbranch(res,loop_blk,after_loop)

        # Build the loop block now.
        bundle.builder.position_at_end(loop_blk)
        # codegen the body
        #print self.statements
        fn_returned = False
    
        bundle.loop_blks = {'after_loop':after_loop,
                            'pre_loop':pre_loop,
                            'loop' : loop_blk}

        for s in self.statements:
            if isinstance(s, ReturnStmt):
                fn_returned = True
                s.codegen(bundle)
                break
            elif isinstance(s, BreakStmt):
                s.codegen(bundle)
                fn_returned = True
                break
            else:
                ret = s.codegen(bundle)
            #bundle.builder.position_at_end(loop_blk)
        # Done with the loop block, make a branch back to pre_loop
        if not fn_returned:
            bundle.builder.branch(pre_loop)
        else:
            pass
            #bundle.builder.branch(after_loop)

        bundle.builder.position_at_end(after_loop)
        bundle.scope.pop_scope()
        bundle.loop_blks = {}

class ForStmt(Statement):
    '''
    Class representing for statement.
    init: initializing expression
    cond: Condition for entering if.
    post_exp: Expression to evaluate at the end of one iteration
    stmts: statements inside if.
    '''
    def __init__(self, init, cond, post_exp, stmts):
        super(ForStmt,self).__init__()
        self.init = init
        self.condition = cond 
        self.post_exp = post_exp
        self.statements = stmts

    def __str__(self):
        return "ForStmt(id=%s, \ninit=%s, \ncondition=%s, \npost_exp=%s, \nstatements=%s)" %\
            (self.node_id, self.init, self.condition, self.post_exp, self.statements)

#    def accept(self, visitor):
#        if visitor.visit_ForStmt(self):
#            for decl in self.stmts:
#                decl.accept(visitor)

class IOLib:
    initialized = False
    @staticmethod
    def initIO(bundle):
        if IOLib.initialized:
            return

        # Permanently load the IOlib so in current directory
        import os
        path = os.getcwd()+"/"+"decaf_io_helpers.so"
        llvm.core.load_library_permanently(path)

        # Make sure that puts has been declared.
        try:
            bundle.module.get_function_named("puts")
        except:
            puts_t = llvm_core.Type.function(llvm_core.Type.int(),[llvm_core.Type.pointer(llvm_core.Type.int(8))])
            puts = bundle.module.add_function(puts_t,"puts")

        try:
            bundle.module.get_function_named("prints")
        except:
            puts_t = llvm_core.Type.function(llvm_core.Type.int(),[llvm_core.Type.pointer(llvm_core.Type.int(8))])
            puts = bundle.module.add_function(puts_t,"prints")

        try:
            bundle.module.get_function_named("print_i")
        except:
            print_i_t = llvm_core.Type.function(llvm_core.Type.int(),[llvm_core.Type.int()],False)
            print_i = bundle.module.add_function(print_i_t,"print_i")

        try:
            bundle.module.get_function_named("print_float")
        except:
            print_f_t = llvm_core.Type.function(llvm_core.Type.int(),[llvm_core.Type.float()],False)
            print_i = bundle.module.add_function(print_f_t,"print_float")
        IOLib.initialized = True

class PrintStmt(Statement):
    '''
    exps: expressions to print
    '''
    def __init__(self, exps):
        super(PrintStmt,self).__init__()
        self.exprs = exps

    def __str__(self):
        return "PrintStmt(id=%s, \nexprs=%s)" %\
            (self.node_id, self.exprs)
    
    def _codegen_print(self,val,bundle):
        # Make sure that all io functions have been initialised and loaded
        IOLib.initIO(bundle)
        if isinstance(val.type, llvm_core.PointerType) and val.type.pointee== llvm_core.Type.int(8):
            # handle printing of a string
            prints = bundle.module.get_function_named("prints")
            return bundle.builder.call(prints,[val],"prints_r")
            
        elif isinstance(val.type, llvm_core.IntegerType):
            # handle printing of integers
            #print "printing integer";
            print_i = bundle.module.get_function_named("print_i")
            return bundle.builder.call(print_i,[val],"print_i_r")

        elif str(val.type)=="float":
            #print "Printing float"
            print_fl = bundle.module.get_function_named("print_float")
            return bundle.builder.call(print_fl,[val],"print_float_r")

        else:
            raise Exception("Printing not implemented for type %s"%(val.type))

    def codegen(self,bundle):      
        values = [exp.codegen(bundle) for exp in self.exprs]
        for v in values:
            self._codegen_print(v,bundle)

        empty = Literal(" ","string_literal").codegen(bundle)

        puts = bundle.module.get_function_named("puts")
        return bundle.builder.call(puts,[empty],"puts_r")
    
#    def accept(self, visitor):
#        if visitor.visit_PrintStmt(self):
#            for decl in self.stmts:
#                decl.accept(visitor)

class ReturnStmt(Statement):
    '''
    Class for return statement
    exp: Expression to return after evaluating

    '''
    def __init__(self, exp):
        super(ReturnStmt,self).__init__()
        self.expr = exp

    def __str__(self):
        return "ReturnStmt(id=%s, \nexpr=%s)" %\
            (self.node_id, self.expr)
    
    def codegen(self,bundle):
        ret_val = self.expr.codegen(bundle)
        bundle.builder.ret(ret_val)
        return ret_val

class BreakStmt(Statement):
    '''
    Class representing Break Statement
    '''
    def __init__(self):
        super(BreakStmt,self).__init__()
        pass

    def __str__(self):
        return "BreakStmt(id=%s)" % (self.node_id)

    def codegen(self,bundle):
        if not bundle.loop_blks:
            raise Exception("Loop blocks not found in bundle. Possible break statement outside a loop")
        else:
            return bundle.builder.branch(bundle.loop_blks['after_loop'])


class ReadIntegerStmt(Statement):
    '''
    Class representing ReadInteger call in decaf.
    '''
    def __init__(self):
        super(ReadIntegerStmt,self).__init__()
        pass

    def __str__(self):
        return "ReadIntegerStmt(id=%s)" % (self.node_id)

class ReadLineStmt(Statement):
    '''
    Class representing ReadLine call in decaf.
    '''
    def __init__(self):
        super(ReadLineStmt,self).__init__()
        pass

    def __str__(self):
        return "ReadLineStmtStmt(id=%s)" % (self.node_id)


class NewStmt(Statement):
    '''
    Class representing 'New' keyword.
    type: Type of the object to be created
    '''
    def __init__(self, type):
        super(NewStmt,self).__init__()
        self.type = type
    
    def __str__(self):
        return "NewStmt(id=%s,type=%s)" % (self.node_id,self.type)

class Literal(Node):
    '''
        Class to represent the literal values.
        Types of literals are 
        boolean
        integer
        double
        string
    '''
    def __init__(self, value, l_type):
        super(Literal,self).__init__()
        self.literal_type = l_type
        self.type = l_type
        self.value = value

    def __str__(self):
        return "id : %s , type : %s, value=%s)" % (self.node_id,self.literal_type,self.value)

    def codegen(self,bundle):
        #print "lit_type, " ,self.literal_type
        #print self.value
        # return appropriate llvm constant
        import re
        # LNUMBER - integer literal
        from llvm.core import Constant as ll_constant
        if self.literal_type == "LNUMBER":
            return ll_constant.int(llvm_core.Type.int(),self.value.strip())
        # DNUMBER - double literal
        if self.literal_type == "DNUMBER":
            return ll_constant.real(llvm_core.Type.float(),self.value.strip())
        # boolean
        elif self.literal_type == "true":
            return ll_constant.int(llvm_core.Type.int(1),1)
        elif self.literal_type == "false":
            return ll_constant.int(llvm_core.Type.int(1),0)
        # string literal
        elif self.literal_type == "string_literal":
            #An adhoc style is being used for string literals
            # All string literals are stored as global constants
            # and a pointer to them is returned 
            self.value = self.value.strip().strip('"')

            #print "string literal ",  self.value
            if bundle.constants.has_key(self.value):
                return bundle.constants[self.value][1]

            # if constant doesnt already exist, create a global constant variable for it and return the pointer                
            const_data = ll_constant.stringz(self.value)
            gvar = bundle.module.add_global_variable(const_data.type,"string_literal")
            gvar.initializer = const_data;
            zero = llvm_core.Constant.int(llvm_core.Type.int(),0)
            ptr = gvar.gep([zero, zero])
            bundle.constants[self.value] = [gvar,ptr]
            return ptr
        # Null
        elif self.literal_type == "null":
            return ll_constant.ConstantPointerNull()

            

class NewArrayStmt(Statement):
    '''
    Class representing 'NewArray' keyword.
    num: Number of objects to be created. Can be an expression, or literal, or variable.
    type: type of which the array is being created. INT, INT[], etc
    '''
    def __init__(self, num, type):
        super(NewArrayStmt,self).__init__()
        self.num = num 
        self.type = type

    def __str__(self):
        return "NewArrayStmt(id=%s)" % (self.node_id) 

class Expression(Node):
    '''
    '''
    # Must call superclass constructor whenever __init__ is overriden

class NameExp(Expression):
    '''
    Class representing NAME
    value: The name of a class, variable etc.
    '''
    def __init__(self, value, type=None):
        super(NameExp,self).__init__()
        self.value = value
        self.type = type
    
    def __str__(self):
        return "NameExp(id=%s, \nvalue=%s)" % (self.node_id, self.value)

    def codegen(self,bundle):
        if not bundle.scope.has_name(self.value):
            raise Exception("Reference to undeclared variable %s"%(self.value))
        # load the value from the memory into a temp
        # return that temp
        return bundle.builder.load(bundle.scope.get(self.value),self.value)

class ArrayNameExpr(Expression):
    '''
    This class represents an array name.
    name: name of the variable 
    index: index 
    '''
    def __init__(self, value, index=None):
        super(ArrayNameExpr,self).__init__()
        self.value = value
        self.index = index
    
    def __str__(self):
        return "ArrayNameExpr(id=%s, \nvalue=%s, \nindex=%s)" % (self.node_id, self.value, self.index)


class IncrementExpr(Expression):
    '''
    Class representing increment Operator
    name: name to be incremented. ++
    '''
    def __init__(self, name):
        super(IncrementExpr,self).__init__()
        self.name = name

    def __str__(self):
        return "IncrementExpr(id=%s, \nname=%s)" % (self.node_id, self.name)

class DecrementExpr(Expression):
    '''
    Class representing decrement Operator
    name: name to be decremented. --
    '''
    def __init__(self, name):
        super(DecrementExpr,self).__init__()
        self.name = name

    def __str__(self):
        return "DecrementExpr(id=%s, \nname=%s)" % (self.node_id, self.name)

class UnaryExpr(Expression):
    '''
    This class defines a node for Unary Expressions.
    operator: Operation i.e. MINUS, NOT etc 
    operand: operand
    '''
    def __init__(self, operator, operand ):
        super(UnaryExpr,self).__init__()
        self.operator = operator
        self.operand = operand

    def __str__(self):
        return "UnaryExpr(id=%s, \noperator=%s, \noperand=%s)" % \
            (self.node_id, self.operator, self.operand)

class ArithmeticExpr(Expression):
    '''
    This class defines a node for an Arithmetic Expression.
    op: Operation i.e. SUM, MINUS etc 
    left: Left operand
    right: Right operand 
    '''
    def __init__(self, op, left, right):
        super(ArithmeticExpr,self).__init__()
        self.left = left
        self.right = right
        self.op = op

    def __str__(self):
        return "ArithmeticExpr(id=%s, \nleft=%s, \nright=%s, \noperator=%s)" %\
            (self.node_id, self.left, self.right, self.op)

    def _codegen_int(self,left,right,bundle):
        op = self.op.strip()
        if op == "+":
            return bundle.builder.add(left,right,"addtmp")
        elif op == "-":
            return bundle.builder.sub(left,right,"subtmp")
        elif op == "*":
            return bundle.builder.mul(left,right,"multmp")
        elif op == "/":
            return bundle.builder.sdiv(left,right,"divtmp")
        else:
            raise Exception("%s not implemented for integers"%(op,))

    def _codegen_float(self,left,right,bundle):
        op = self.op.strip()
        if op == "+":
            return bundle.builder.fadd(left,right,"faddtmp")
        elif op == "-":
            return bundle.builder.fsub(left,right,"fsubtmp")
        elif op == "*":
            return bundle.builder.fmul(left,right,"fmultmp")
        elif op == "/":
            return bundle.builder.fdiv(left,right,"fdivtmp")
        else:
            raise Exception("%s not implemented for floating points"%(op,))

       

    def codegen(self, bundle):
        left = self.left.codegen(bundle)
        right = self.right.codegen(bundle)

        op = self.op.strip()

        supported_ops = ["+","*","-","/"]
        if not op in supported_ops:
            raise Exception("%s operation not implemented yet"%(self.op,))
        # If the types for left and right dont match, convert ints to doubles.
        if not left.type == right.type:
            #cooerce non floats to floats
            if isinstance(left.type, llvm_core.IntegerType):
                left = bundle.builder.sitofp(left,llvm_core.Type.float())
            if isinstance(right.type, llvm_core.IntegerType):
                right = bundle.builder.sitofp(right,llvm_core.Type.float())
        if isinstance(left.type, llvm_core.IntegerType):
            return self._codegen_int(left,right,bundle)
        elif str(left.type) == 'float':
            return self._codegen_float(left, right, bundle)
        else:
            raise Exception("Binary ops implemented only for ints ryt now")

class ConditionalExpr(Expression):
    '''
    This class defines a node for Conditional Expressions.
    op: Operation i.e. IS_SMALLER, IS_GREATER etc 
    left: Left operand
    right: Right operand 
    '''
    def __init__(self, op, left, right):
        super(ConditionalExpr,self).__init__()
        self.left = left
        self.right = right
        self.op = op

    def __str__(self):
        return "ConditionalExpr(id=%s, \nleft=%s, \nright=%s, \noperator=%s)" %\
            (self.node_id, self.left, self.right, self.op)

    import llvm.core as lc
    operators = {
        '==' : {'int':lc.ICMP_EQ, 'float': lc.FCMP_OEQ }, 
        '!=' : {'int':lc.ICMP_NE , 'float': lc.FCMP_ONE},
        '<'  : {'int':lc.ICMP_SLT,'float':lc.FCMP_OLT},
        '>'  : {'int':lc.ICMP_SGT,'float': lc.FCMP_OGT},
        '>=' : {'int':lc.ICMP_SLE, 'float': lc.FCMP_OLE},
        '<=' : {'int':lc.ICMP_SGE, 'float': lc.FCMP_OGE},
        }

    def _codegen_integer(self,left,right,bundle):
        self.op = self.op.strip()
        op = ConditionalExpr.operators[self.op]['int']
        return bundle.builder.icmp(op,left,right,"integer_comp")

    def _codegen_float(self,left,right,bundle):
        self.op = self.op.strip()
        op = ConditionalExpr.operators[self.op]['float']
        return bundle.builder.fcmp(op,left,right)

    def codegen(self, bundle):
        left = self.left.codegen(bundle)
        right = self.right.codegen(bundle)
        if not left.type == right.type:
            raise Exception("The two operands to a binary comparision must have the same type.")
        if isinstance(left.type, llvm_core.IntegerType):
            return self._codegen_integer(left,right,bundle)
        elif str(left.type) == "float":
            return self._codegen_float(left,right,bundle)
        else:
            raise Exception("Invalid types for conditional operator %s"%(str(left.type),))

class CallExpr(Expression):
    '''
    Class representing calling a function.
    expr: Expression/Object that calls the function.
    name: name of the function
    act_params: Actual parameters passed.
    '''
    def __init__(self, name, act_params, expr=None):
        super(CallExpr,self).__init__()
        self.expr = expr
        self.name = name
        self.act_params = act_params  # this is in form of a list
    
    def __str__(self):
        return "CallExpr(id=%s, \nexpr=%s, \nname=%s, \nact_params=%s)" % \
            (self.node_id, self.expr, self.name, self.act_params)

    def codegen(self,bundle):
        # make sure that the function exists
        if not bundle.scope.has_name(self.name.value):
            raise Exception("Cannot call undefined function %s"%(self.name.value,))
        callee = bundle.scope.get(self.name.value)
        if len(callee.args) != len(self.act_params):
            raise Exception("Number of arguments supplied dont match the function signature for %s"%(self.name.value,))
        args = [arg.codegen(bundle) for arg in self.act_params]

        return bundle.builder.call(callee,args, "calltmp")
