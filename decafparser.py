#!/usr/bin/env python

import os
import sys
import ast
from lexers import CaffeineLexer as CL
import decafast as ast
import ply.yacc as yacc
from decafast import *

precedence = (
    ('left', 'COMMA'),
    ('right', 'PRINT'),
    ('left', 'EQUALS'),
    ('left', 'COLON'),
    ('left', 'BOOLEAN_OR'),
    ('left', 'BOOLEAN_AND'),
    ('nonassoc', 'IS_EQUAL', 'IS_NOT_EQUAL'),
    ('nonassoc', 'IS_SMALLER', 'IS_SMALLER_OR_EQUAL', 'IS_GREATER', 'IS_GREATER_OR_EQUAL'),
    ('left', 'PLUS', 'MINUS', 'CONCAT'),
    ('left', 'MUL', 'DIV', 'MOD'),
    ('right', 'BOOLEAN_NOT'),
    ('right', 'LBRACKET'),
    ('nonassoc', 'NEW'),
    ('left', 'ELSE'),
)

class ExpressionParser(object):
    def p_expression(self, p):
        '''expression : assignment_expression
                      | lvalue'''
        p[0] = p[1]

    def p_assignment_expression(self, p):
        '''assignment_expression : assignment'''
        p[0] = p[1]

    def p_assignment(self, p):
        '''assignment : lvalue EQUALS expression'''

    def p_lvalue(self, p):
        '''lvalue : name'''

    def p_expression_binary_op(self, p):
        '''expression : expression BOOLEAN_AND expression
                      | expression BOOLEAN_OR expression
                      | expression PLUS expression
                      | expression MINUS expression
                      | expression MUL expression
                      | expression DIV expression
                      | expression MOD expression
                      | expression IS_EQUAL expression
                      | expression IS_NOT_EQUAL expression
                      | expression IS_SMALLER expression
                      | expression IS_SMALLER_OR_EQUAL expression
                      | expression IS_GREATER expression
                      | expression IS_GREATER_OR_EQUAL expression'''
        pass

    def p_expression_unary_op(self, p):
        '''expression : PLUS expression 
                      | MINUS expression
                      | BOOLEAN_NOT expression'''
        pass

    def p_statement_expression(self, p):
        '''statement_expression : assignment
                                | method_invocation
                                | class_instance_creation_expression'''                  
        pass

    def p_expression_literal(self, p):
        '''expression : literal
                      | THIS'''



class ClassParser(object):
    def p_program(self, p):
        '''program : declaration'''
        pass
                            
    def p_declaration(self, p):
        '''declaration : field_declaration
                       | method_declaration
                       | class_declaration
                       | interface_declaration'''
        # ignore
        pass

    def p_class_declaration(self, p):
        '''class_declaration : class_header class_body'''
        p[0] = ClassDecl(p[1]['name'], p[2],extends=p[1]['extends'], implements=p[1]['implements'])

    def p_class_header(self, p):
        '''class_header : class_header_name class_header_extends_opt class_header_implements_opt'''
        p[1]['extends'] = p[2]
        p[1]['implements'] = p[3]
        p[0] = p[1]

    def p_class_header_name(self, p):
        '''class_header_name : CLASS NAME '''
	p[0] = {'name': p[2]}

    def p_class_header_extends_opt(self, p):
        '''class_header_extends_opt : class_header_extends
                                    | empty'''
	p[0] = p[1]

    def p_class_header_extends(self, p):
        '''class_header_extends : EXTENDS class_type'''
        p[0] = p[2]
	
    def p_class_header_implements_opt(self, p):
        '''class_header_implements_opt : class_header_implements
                                       | empty'''
        p[0] = p[1]

    def p_class_header_implements(self, p):
        '''class_header_implements : IMPLEMENTS interface_type_list'''
        p[0] = p[2]

    def p_interface_type_list(self, p):
        '''interface_type_list : interface_type
                               | interface_type_list COMMA interface_type'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]

    def p_interface_type(self, p):
        '''interface_type : class_or_interface_type'''
        p[0] = p[1]

    def p_class_body(self, p):
        '''class_body : LBRACE class_body_declarations_opt RBRACE '''
        p[0] = p[2]            

    def p_class_body_declarations_opt(self, p):
        '''class_body_declarations_opt : class_body_declarations'''
        p[0] = p[1]

    def p_class_body_declarations_opt2(self, p):
        '''class_body_declarations_opt : empty'''
        p[0] = []

    def p_class_body_declarations(self, p):
        '''class_body_declarations : class_body_declaration
                                   | class_body_declarations class_body_declaration'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]

    def p_class_body_declaration(self, p):
        '''class_body_declaration : class_member_declaration'''
        p[0] = p[1]

    def p_class_body_declaration2(self, p):
        '''class_body_declaration : block'''
        p[0] = ClassInitializer(p[1])

    def p_class_member_declaration(self, p):
        '''class_member_declaration : field_declaration
                                    | method_declaration'''
    	p[0] = p[1]

    def p_class_member_declaration2(self, p):
        '''class_member_declaration : SEMICOLON '''
        # ignore
        pass

    def p_field_declaration(self, p):
        '''field_declaration : type variable_declarators SEMICOLON '''
        

    def p_method_declaration(self, p):
        '''method_declaration : method_header method_body'''
        

    def p_method_body(self, p):
        '''method_body : LBRACE block_statements_opt RBRACE '''
        p[0] = p[2]

    def p_method_header(self, p):
        '''method_header : method_header_name LPAREN formal_parameter_list_opt RPAREN'''
        p[1]['parameters'] = p[3]
        p[0] = p[1]

    def p_method_header_name(self, p):
        '''method_header_name :  type NAME'''
        p[0] = {'type': p[1], 'name': p[2]}
	
    def p_formal_parameter_list_opt(self, p):
        '''formal_parameter_list_opt : formal_parameter_list'''
        p[0] = p[1]

    def p_formal_parameter_list_opt2(self, p):
        '''formal_parameter_list_opt : empty'''
        p[0] = []

    def p_formal_parameter_list(self, p):
        '''formal_parameter_list : formal_parameter
                                 | formal_parameter_list COMMA formal_parameter'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]

    def p_formal_parameter(self, p):
        '''formal_parameter : type variable_declarator_id'''
        p[0] = FormalParameter(p[2], p[1])

    def p_interface_declaration(self, p):
        '''interface_declaration : interface_header interface_body'''
        p[0] = InterfaceDeclaration(p[1],
                                    extends=p[1]['extends'],
                                    body=p[2])

    def p_interface_header(self, p):
        '''interface_header : interface_header_name interface_header_extends_opt'''
        p[1]['extends'] = p[2]
        p[0] = p[1]

    def p_interface_header_name(self, p):
        '''interface_header_name : interface_header_name1'''
        p[0] = p[1]

    def p_interface_header_name1(self, p):
        '''interface_header_name1 : INTERFACE NAME'''
        p[0] = {'name': p[2]}

    def p_interface_header_extends_opt(self, p):
        '''interface_header_extends_opt : interface_header_extends'''
        p[0] = p[1]

    def p_interface_header_extends_opt2(self, p):
        '''interface_header_extends_opt : empty'''
        p[0] = []

    def p_interface_header_extends(self, p):
        '''interface_header_extends : EXTENDS interface_type_list'''
        p[0] = p[2]

    def p_interface_body(self, p):
        '''interface_body : LBRACE interface_member_declarations_opt RBRACE '''
        p[0] = p[2]

    def p_interface_member_declarations_opt(self, p):
        '''interface_member_declarations_opt : interface_member_declarations'''
        p[0] = p[1]

    def p_interface_member_declarations_opt2(self, p):
        '''interface_member_declarations_opt : empty'''
        p[0] = []

    def p_interface_member_declarations(self, p):
        '''interface_member_declarations : interface_member_declaration
                                         | interface_member_declarations interface_member_declaration'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]
        
    def p_interface_member_declaration(self, p):
        '''interface_member_declaration : constant_declaration
                                        | method_declaration'''
        p[0] = p[1]

    def p_interface_member_declaration2(self, p):
        '''interface_member_declaration : SEMICOLON '''
        # ignore

    def p_constant_declaration(self, p):
        '''constant_declaration : field_declaration'''
        p[0] = p[1]

    def p_class_instance_creation_expression(self, p):
        '''class_instance_creation_expression : NEW class_type'''
        



class TypeParser(object):

    def p_type(self, p):
        '''type : primitive_type
                | reference_type'''
        p[0] = p[1]

    def p_primitive_type(self, p):
        '''primitive_type : BOOL
                          | VOID
                          | INT
                          | DOUBLE
                          | STRING'''
        p[0] = p[1]

    def p_reference_type(self, p):
        '''reference_type : class_or_interface_type'''
        p[0] = p[1]

    def p_class_or_interface_type(self, p):
        '''class_or_interface_type : class_or_interface'''
        p[0] = p[1]

    def p_class_type(self, p):
        '''class_type : class_or_interface_type'''
        p[0] = p[1]

    def p_class_or_interface(self, p):
        '''class_or_interface : name'''
        p[0] = Type(p[1])

class LiteralParser(object):

    def p_literal(self, p):
        '''literal : DNUMBER
                   | LNUMBER
                   | STRING_LITERAL
                   | TRUE
                   | FALSE
                   | NULL'''
        p[0] = Literal(p[1])  


class NameParser(object):

    def p_name(self, p):
        '''name : simple_name
                | qualified_name'''
        p[0] = p[1]

    def p_simple_name(self, p):
        '''simple_name : NAME'''

    def p_qualified_name(self, p):
        '''qualified_name : name DOT simple_name'''
        p[1].append_name(p[3])
        p[0] = p[1]              

class StatementParser(object):
    def p_block(self, p):
        '''block : LBRACE block_statements_opt RBRACE '''
        p[0] = Block(p[2])

    def p_block_statements_opt(self, p):
        '''block_statements_opt : block_statements'''
        p[0] = p[1]

    def p_block_statements_opt2(self, p):
        '''block_statements_opt : empty'''
        p[0] = []

    def p_block_statements(self, p):
        '''block_statements : block_statement
                            | block_statements block_statement'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[2]]

    def p_block_statement(self, p):
        '''block_statement : local_variable_declaration_statement
                           | statement'''
        p[0] = p[1]

    def p_local_variable_declaration_statement(self, p):
        '''local_variable_declaration_statement : local_variable_declaration SEMICOLON '''
        p[0] = p[1]

    def p_local_variable_declaration(self, p):
        '''local_variable_declaration : type variable_declarators'''
        

    def p_variable_declarators(self, p):
        '''variable_declarators : variable_declarator
                                | variable_declarators COMMA variable_declarator'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]

    def p_variable_declarator(self, p):
        '''variable_declarator : variable_declarator_id
                               | variable_declarator_id EQUALS variable_initializer'''


    def p_variable_declarator_id(self, p):
        '''variable_declarator_id : NAME'''
        

    def p_variable_initializer(self, p):
        '''variable_initializer : expression'''
        p[0] = p[1]        

    def p_statement(self, p):
        '''statement : statement_without_trailing_substatement
                     | if_statement
                     | if_else_statement
                     | while_statement
                     | for_statement'''
        p[0] = p[1]

    def p_statement_without_trailing_substatement(self, p):
        '''statement_without_trailing_substatement : block
                                                   | expression_statement
                                                   | empty_statement
                                                   | break_statement
                                                   | return_statement'''
        p[0] = p[1]

    def p_expression_statement(self, p):
        '''expression_statement : statement_expression SEMICOLON'''
        p[0] = p[1]        	

    def p_method_invocation(self, p):
        '''method_invocation : name LPAREN argument_list_opt RPAREN '''
        p[0] = MethodInvocation(p[1], arguments=p[3])

    def p_method_invocation2(self, p):
        '''method_invocation : name '.'  NAME LPAREN argument_list_opt RPAREN'''
        p[0] = MethodInvocation(p[3], target=p[1], arguments=p[5])

    def p_argument_list_opt(self, p):
        '''argument_list_opt : argument_list'''
        p[0] = p[1]

    def p_argument_list_opt2(self, p):
        '''argument_list_opt : empty'''
        p[0] = []

    def p_argument_list(self, p):
        '''argument_list : expression
                         | argument_list COMMA expression'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]

    def p_if_statement(self, p):
        '''if_statement : IF LPAREN expression RPAREN statement'''
        p[0] = IfThenElse(p[3], p[5])

    def p_if_else_statement(self, p):
        '''if_else_statement : IF LPAREN expression RPAREN statement ELSE statement'''
        p[0] = IfThenElse(p[3], p[5], p[7])

    def p_while_statement(self, p):
        '''while_statement : WHILE LPAREN expression RPAREN statement'''
        p[0] = While(p[3], p[5])        

    def p_for_statement(self, p):
        '''for_statement : FOR LPAREN for_init_opt SEMICOLON expression_opt SEMICOLON for_update_opt SEMICOLON statement'''
        p[0] = For(p[3], p[5], p[7], p[9])

    def p_for_init_opt(self, p):
        '''for_init_opt : for_init
                        | empty'''
        p[0] = p[1]

    def p_for_init(self, p):
        '''for_init : statement_expression_list
                    | local_variable_declaration'''
        p[0] = p[1]

    def p_statement_expression_list(self, p):
        '''statement_expression_list : statement_expression
                                     | statement_expression_list COMMA statement_expression'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]

    def p_expression_opt(self, p):
        '''expression_opt : expression
                          | empty'''
        p[0] = p[1]

    def p_for_update_opt(self, p):
        '''for_update_opt : for_update
                          | empty'''
        p[0] = p[1]

    def p_for_update(self, p):
        '''for_update : statement_expression_list'''
        p[0] = p[1]  

    def p_empty_statement(self, p):
        '''empty_statement : SEMICOLON'''
        # ignore       

    def p_return_statement(self, p):
        '''return_statement : RETURN expression_opt SEMICOLON '''
        p[0] = Return(p[2])    

    def p_break_statement(self, p):
        '''break_statement : BREAK SEMICOLON'''
        p[0] = Break()                        	

class DecafParser(NameParser, LiteralParser, TypeParser, ClassParser, StatementParser, ExpressionParser):
    # Get the token map
    tokens = CL.tokens
    def p_expression_print(self, p):
        '''expression : PRINT expression'''
    
    # Error rule for syntax errors
    def p_error(self, t):
        print("Illegal character '{}' ({}) in line {}".format(t.value[0], hex(ord(t.value[0])), t.lexer.lineno))
        print ("Error {}".format(t))
        if t:
            raise SyntaxError('invalid syntax', (None, t.lineno, None, t.value))
        else:
            raise SyntaxError('unexpected EOF while parsing', (None, None, None, None))

    def p_empty(self, p):
        '''empty :'''

if __name__ == '__main__':
    import readline
    import pprint
    s = ''
    clex = CL()
    clex.build()
    lexer = clex.lexer
    # Build the grammar
    parser = yacc.yacc(module=DecafParser(), write_tables=0, start='program')

    while True:
       try:
           if s:
               prompt = '     '
           else:
               prompt = lexer.current_state()
               if prompt == 'INITIAL': prompt = 'decaf'
               prompt += '> '
           s += raw_input(prompt)
       except EOFError:
           break
       if not s: continue
       s += '\n'
       try:
           lexer.lineno = 1
           result = parser.parse(s, lexer=lexer)
       except SyntaxError, e:
           if e.lineno is not None:
               print e, 'near', repr(e.text)
               s = ''
           continue
       if result:
           for item in result:
               if hasattr(item, 'generic'):
                   item = item.generic()
               pprint.pprint(item)
       s = ''
