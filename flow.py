from visitor import Visitor
from typechecker import SymbolTable

#  FLOW CONTROL, SEMANTIC ANALYSIS

class FlowAnalyser(Visitor):
    """Performs flow control checking on the AST.  This makes sure
    that functions return properly through all branches, that
    break/continue statements are only present within loops, and so
    forth."""
    
    def visit_Node(self, node):
        node.has_return_stmt = 0

    def visit_Program(self, nodes):
        self.vNodeList(nodes)
        return nodes

    def vStatementList(self, node):
        node.has_return_stmt = 0
        for stmt in node.nodes:
            if node.has_return_stmt:
                self.warning("Function %s has at least one unreachable statement." % self.curr_func.name)
            stmt.accept(self)
            if stmt.has_return_stmt:
                node.has_return_stmt = 1

    def visit_WhileStmt(self, node):
        old_in_loop = self.in_loop
        self.in_loop = 1
        #self.visitSubNodes(node.statements)
        node.has_return_stmt = node.has_return_stmt()
        self.in_loop = old_in_loop

    def visit_ForStmt(self, node):
        self.visit_WhileStmt(node)

    def visit_BreakStmt(self, node):
        node.has_return_stmt = 0        
        if not self.in_loop:
            self.error("Break statement outside of loop.")

    def visit_IfStmt(self, node):
#        node.else_stmts.accept()
        self.visitSubNodes(node.statements)
        self.visitSubNodes(node.else_stmts)
        if node.has_return() and node.else_has_return():
            node.has_return_stmt = 1
        else:
            node.has_return_stmt = 0
            
    def visit_FunctionDecl(self, node):
        self.curr_func = node
        self.in_loop = 0
        self.visitSubNodes(node.body)
        node.has_return_stmt = node.has_return()
        if not node.has_return_stmt:
            self.error("Function %s doesn't return through all branches." % node.name)

    def visit_ReturnStmt(self, node):
        node.has_return_stmt = 1

    def vCompoundStatement(self, node):
        node.statement_list.accept(self)
        node.has_return_stmt = node.statement_list.has_return_stmt
