from visitor import Visitor
from decafast import Type, NameExp, VariableDecl
#---------------------------------------------------------
#
#  File handles type-checking, semantic analysis, scoping
#
#----------------------------------------------------------

#  ---------------------------------------------------------------
#  SYMBOL TABLE GENERATION
#  ---------------------------------------------------------------

class SymbolTable:
    """A symbol table.  This is a simple object that just keeps a
    hashtable of symbol names and the Declaration or FunctionDefn
    nodes that they refer to.

    There is a separate symbol table for each code element that
    has its own scope (for instance, each compound statement will
    have its own symbol table).  As a result, symbol tables can
    be nested if the code elements are nested, and symbol table
    lookups will recurse upwards through parents to represent
    lexical scoping rules."""

    class SymbolDefinedError(Exception):
        """Exception raised when the code tries to add a symbol
        to a table where the symbol has already been defined.
        """

        pass

    class SymbolConflictError(Exception):
        """Exception raised when the code tries to add a
        symbol to a table where the symbol already exists
        and its type differs from the previously existing
        one."""
        
        pass

    def __init__(self, parent=None):
        """Creates an empty symbol table with the given
        parent symbol table."""

        self.entries = {}
        self.parent = parent
        if self.parent != None:
            self.parent.children.append(self)
        self.children = []
    
    def add(self, name, value):
        """Adds a symbol with the given value to the symbol table.
        The value is usually an AST node that represents the
        declaration or definition of a function/variable (e.g.,
        Declaration or FunctionDefn)."""
        if name in self.entries:
            if(type(value) != type(self.entries[name])):
                raise SymbolTable.SymbolConflictError()
            else:
                if( 'Variable' in str(type(value)) and (value.type.type != self.entries[name].type.type) ):
                    raise SymbolTable.SymbolConflictError()
                else:
                    raise SymbolTable.SymbolDefinedError()
        self.entries[name] = value

    def get(self, name):
        """Retrieves the symbol with the given name from the symbol
        table, recursing upwards through parent symbol tables if it is
        not found in the current one."""

        if self.entries.has_key(name):
            return self.entries[name]
        else:
            if self.parent != None:
                return self.parent.get(name)
            else:
                return None

    def print_symbols(self):
        """ Function to get list of all the symbols in the current
        table.
        """
        print('Symbols in %s -' % self)
        for key in self.entries.keys():
            print(key)
#            print(self.entries[key])
        print('\nChildren of %s - ' % self)
        for child in self.children:
            child.print_symbols()
        print('Children of %s over!' % self)

class SymbolTableCreator(Visitor):
    """Visitor that creates and attaches symbol tables to the AST."""
    
    def push_symtab(self, node):
        """Pushes a new symbol table onto the visitor's symbol table
        stack and attaches this symbol table to the given node.  This
        is used whenever a new lexical scope is encountered, so the
        node is usually a CompoundStatement object."""

        self.curr_symtab = SymbolTable(self.curr_symtab)
        node.symtab = self.curr_symtab

    def pop_symtab(self):
        """Pops a symbol table off the visitor's symbol table stack.
        This is used whenever a new lexical scope is exited."""
        
        self.curr_symtab = self.curr_symtab.parent

    def visit_Program(self, nodes):
        self.root_symtab = SymbolTable()
        self.curr_symtab = self.root_symtab
        self.vNodeList(nodes)
        return self.root_symtab, nodes

    def visit_Node(self, node):
        pass

    def visit_ClassDecl(self, node):
        self._add_symbol(node)
        self.visit_NameExp(node.name)
        self.push_symtab(node)
        self.visitSubNodes(node.body)
        self.pop_symtab()

    def visit_InterfaceDecl(self, node):
        self._add_symbol(node)
        self.visit_NameExp(node.name)
        self.push_symtab(node)
        self.visitSubNodes(node.body)
        self.pop_symtab()

    def visit_FunctionDecl(self, node):
        self._add_symbol(node)
        self.visit_NameExp(node.name)
        self.push_symtab(node)
        self.visitSubNodes(node.params)
        self.visitSubNodes(node.body)
        self.pop_symtab()

    def visit_PrototypeDecl(self, node):
        pass

    def visit_Type(self, node):
        pass

    def visit_VariableDecl(self, node):
        self._add_symbol(node)  # not thought about array
        self.visit_NameExp(node.name)

    def visit_Statement(self, node):
        pass

    def visit_AssignmentStmt(self, node):
        if not node.lhs is None:
            node.lhs.accept(self)
        if not node.rhs is None:
            node.rhs.accept(self)

    def visit_IfStmt(self, node):
        node.condition.accept(self)
        self.visitSubNodes(node.statements)
        self.visitSubNodes(node.else_stmts)

    def visit_WhileStmt(self, node):
        node.condition.accept(self)
        self.visitSubNodes(node.statements)

    def visit_ForStmt(self, node):
        if not node.init is None:        
            node.init.accept(self)
        if not node.condition is None:
            node.condition.accept(self)
        if not node.post_exp is None:        
            node.post_exp.accept(self)
        if not node.statements is None:
            self.visitSubNodes(node.statements)

    def visit_PrintStmt(self, node):
        self.visitSubNodes(node.exprs)

    def visit_ReturnStmt(self, node):
        if not node.expr is None:        
            node.expr.accept(self)

    def visit_BreakStmt(self, node):
        pass

    def visit_ReadIntegetStmt(self, node):
        pass

    def visit_ReadLineStmt(self, node):
        pass

    def visit_NewStmt(self, node):
        node.type = node.type.value

    def visit_Literal(self, node):
        pass

    def visit_NewArrayStmt(self, node):
        pass

    def visit_NameExp(self, node):
        if node.value in self.curr_symtab.entries.keys():
            decl_node = self.curr_symtab.entries[node.value]
            if isinstance(decl_node,VariableDecl):
                node.type = decl_node.type
            else:
                node.type = decl_node.name.value
 
    def visit_ArrayNameExpr(self, node):
        if not node.index is None:
            node.index.accept(self)

    def visit_IncrementExpr(self, node):
        pass

    def visit_DecrementExpr(self, node):
        pass

    def visit_UnaryExpr(self, node):
        if not node.operand is None:
            node.operand.accept(self)

    def visit_ArithmeticExpr(self, node):
        if not node.left is None:
            node.left.accept(self)
        if not node.right is None:
            node.right.accept(self)

    def visit_ConditionalExpr(self,node):
        if not node.left is None:
            node.left.accept(self)
        if not node.right is None:
            node.right.accept(self)

    def visit_CallExpr(self, node):
        if not node.expr is None:
            node.expr.accept(self)
        self.visitSubNodes(node.act_params)

    def visit_Id(self, node):
        symbol = self.curr_symtab.get(node.name)
        if symbol != None:
            node.symbol = symbol
            node.symbol.is_used = 1
            node.set_has_address()
        else:
            self.error("Line %d: Unknown identifier '%s'." % (node.lineno, node.name))

    def _add_symbol(self, node):
        """Attempts to add a symbol for the given node to the current
        symbol table, catching any exceptions that occur and printing
        errors if necessary."""
        
        try:
            self.curr_symtab.add(node.name.value, node)
        except SymbolTable.SymbolDefinedError:
            self.error("Symbol '%s' already defined." % node.name.value)
        except SymbolTable.SymbolConflictError:
            self.error("Symbol '%s' has multiple differing declarations." % node.name.value)
        
#---------------------------------------------------------------
#  TYPE CHECKING
#---------------------------------------------------------------

class TypeChecker(Visitor):
    """Class that performs type checking on the AST, attaching a
    Type object subclass to every eligible node and making sure these
    types don't conflict."""

#    symtab = None # would be set in visit_Program function

    class OperandTypeMismatchError(Exception):
        """Exception raised when the code tries apply two
    different kinds of operands to an operatior.
        """
    pass

    class TypeMismatchError(Exception):
        """Exception raised when the code tries improper value
    to a variable
        """
    pass

    class VariableUndefinedError(Exception):
        """Exception raised when the code tries to use a variable
        that has not been defined
        """
    pass

    def _process_conditional(self, expr):
        """Does simple type checking for an expression that is
        supposed to be the expression for a conditional
        statement (e.g., the conditional clause of an if/then
        statement or a loop)."""
        
        if expr.type.get_outer_string() not in ['int', 'char']:
            self.error("Conditional expression doesn't evaluate to an int/char/etc.")

    def _coerce_consts(self, var1, var2):
        """Looks at two typed terminals to see if one of them
        is a constant integral.  If it is, then coerce it to
        the type of the other terminal.

        Note that both terminals cannot be constant integrals, or else
        they would have already been reduced to one node by the node's
        calculate() method in the parsing stage."""
        
        if var1.is_const():
            self._coerce_const(var1, var2.type)
        elif var2.is_const():
            self._coerce_const(var2, var1.type)

    def _coerce_const(self, var, type):
        """If the given typed terminal is a constant, coerces it to
         the given type."""
        
        if var.is_const() and type.get_string() in ['int', 'char']:
            var.type = type
            
    def _check_const_range(self, var, type):
        """Checks the given integral constant to make sure its value
        is within the bounds of the given type."""
        
        val = var.value
        type_str = type.get_outside_string()
        # TODO: implement this!
        if type_str == 'char':
            pass
        elif type_str == 'int':
            pass

    def reduce_type(self,left):
        if(left == 'LNUMBER'):
            left = Type('int',0)
        if(left == 'DNUMBER'):
            left = Type('double',0)
        if(left == 'string_literal'):
            left = Type('string',0)
        if(left == 'true' or left == 'false'):
            left = Type('bool',0)
        return left

    # left and right passed are
    def match_types(self,left,right):
        left = self.reduce_type(left.type)
        right = self.reduce_type(right.type)
        if(left.type == right.type):
            return True
        else:
            return False

    def _compare_types(self, name_str, from_type, to_type, raise_errors=1):
        """Compares the two types to see if it's possible to perform a
        binary operation on them.  If it is not, then the appropriate
        errors raised, unless raise_errors is set to
        0."""

        ERROR = 2
        conflict = False
        from_str = from_type.get_string()
        to_str = to_type.get_string()
        if (from_str != to_str):
            conflict = True
        if not raise_errors:
            return conflict
        else:    
            self.error("%s: Cannot convert from %s to %s." % (name_str, from_str, to_str))

    def visit_Node(self, node):
        pass

    def visit_Id(self, node):
        node.type = node.symbol.type

    def visit_Program(self, nodes, symtab):
        try:
            self.symtab = symtab
            self._visitList(nodes)
        except TypeChecker.TypeMismatchError :
            self.error("Incompatible types used for assignment!")
        except TypeChecker.OperandTypeMismatchError:
            self.error("Operands of incompatible types used of operation!")

    def visit_Node(self, node):
        pass

    def visit_ClassDecl(self, node):
        self.visitSubNodes(node.body)

    def visit_InterfaceDecl(self, node):
        self.visitSubNodes(node.body)

    def visit_FunctionDecl(self, node):
        self.visitSubNodes(node.body)

    def visit_PrototypeDecl(self, node):
        pass

    def visit_Type(self, node):
        pass

    def visit_VariableDecl(self, node):
        pass

    def visit_Statement(self, node):
        pass

    def visit_AssignmentStmt(self, node):
        if not node.lhs is None:
            node.lhs.accept(self)
        if not node.rhs is None:
            node.rhs.accept(self)
        if not self.match_types(node.lhs,node.rhs) :
            raise TypeChecker.TypeMismatchError()

    def visit_IfStmt(self, node):
        node.condition.accept(self)
        self.visitSubNodes(node.statements)
        self.visitSubNodes(node.else_stmts)

    def visit_WhileStmt(self, node):
        node.condition.accept(self)
        self.visitSubNodes(node.statements)

    def visit_ForStmt(self, node):
        if not node.init is None:        
            node.init.accept(self)
        if not node.condition is None:
            node.condition.accept(self)
        if not node.post_exp is None:        
            node.post_exp.accept(self)
        if not node.statements is None:
            self.visitSubNodes(node.statements)

    def visit_PrintStmt(self, node):
        self.visitSubNodes(node.exprs)

    def visit_ReturnStmt(self, node):
        if not node.expr is None:        
            node.expr.accept(self)

    def visit_BreakStmt(self, node):
        pass

    def visit_ReadIntegetStmt(self, node):
        pass

    def visit_ReadLineStmt(self, node):
        pass

    def visit_NewStmt(self, node):
        pass

    def visit_Literal(self, node):
        pass

    def visit_NewArrayStmt(self, node):
        pass

    def visit_NameExp(self, node):
        pass

    def visit_ArrayNameExpr(self, node):
        if not node.index is None:
            node.index.accept(self)

    def visit_IncrementExpr(self, node):
        pass

    def visit_DecrementExpr(self, node):
        pass

    def visit_UnaryExpr(self, node):
        if not node.operand is None:
            node.operand.accept(self)

    def visit_ArithmeticExpr(self, node):
        if not node.left is None:
            node.left.accept(self)
        if not node.right is None:
            node.right.accept(self)
        if not self.match_types(node.left,node.right) :
            raise TypeChecker.OperandTypeMismatchError()
        else:
            node.type = node.left.type

    def visit_ConditionalExpr(self,node):
        if not node.left is None:
            node.left.accept(self)
        if not node.right is None:
            node.right.accept(self)
        if not self.match_types(node.left,node.right) :
            raise TypeChecker.OperandTypeMismatchError()
        else:
            node.type = node.left.type

    def visit_CallExpr(self, node):
        if not node.expr is None:
            node.expr.accept(self)
        self.visitSubNodes(node.act_params)
