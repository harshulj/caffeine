
class Visitor:
    """The base visitor class.  This is an abstract base class."""

    def __init__(self):
        self.warnings = 0
        self.errors = 0

    def _visitList(self, list):
        """Visit a list of nodes.  'list' should be an actual list,
        not a cparse.NodeList object."""
        from decafast import FunctionDecl, VariableDecl, IfStmt, AssignmentStmt, \
                WhileStmt, ArithmeticExpr, UnaryExpr, ForStmt, PrintStmt, ReturnStmt
        impl = [FunctionDecl, VariableDecl, IfStmt, AssignmentStmt, \
                WhileStmt, ArithmeticExpr, UnaryExpr, ForStmt, PrintStmt, ReturnStmt]
        
        last = None
        for i in list:
            if not i is None:
				to_be_cke = False
				for type in impl:
					if isinstance(i,type):
						to_be_cke = True
				if to_be_cke:
					last = i.accept(self)
        return last
    
    def visit(self, node):
        """Visits the given node by telling the node to call the
        visitor's class-specific visitor method for that node's
        class (i.e., double dispatching)."""
        
        return node.accept(self)

    def vNodeList(self, node):
        self._visitList(node)

    def visitSubNodes(self,node_list):
        self._visitList(node_list)

    def warning(self, str):
        """Output a non-fatal compilation warning."""
        
        print "warning: %s" % str
        self.warnings += 1

    def error(self, str):
        """Output a fatal compilation error."""
        
        print "error: %s" % str
        self.errors += 1
        exit()

    def has_errors(self):
        """Returns whether the visitor has encountered any
        errors."""
        
        return self.errors > 0
