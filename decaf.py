import sys, pydot, ntpath
from lexers import CaffeineLexer as CL
from parser import DecafParser, Scope, CodegenBundle
import ply.yacc as yacc
from decafast import *
from typechecker import SymbolTableCreator
from typechecker import TypeChecker
from flow import FlowAnalyser

class DecafCompiler():
    '''
    This class represents a Decaf compiler.
    '''

    def __init__(self,file=None):
        # Get input from stdin if no file is specified.
        if file == None:
            self.file = file
            self.code = self._get_from_STDIN()
        # Get input from file
        else:
            self.code = self._get_from_File(file)
        # Build the lexer
        self.lexer = self._build_lexer()
        # Build the parser
        self.parser = self._build_parser()
        # Start Compiling. Includes creation of parse tree, semantic analysis
        # and code generation
        self.compile()

    def _get_from_STDIN(self):
        '''
        Get code input from STDIN
        '''
        return sys.stdin.read()

    def _get_from_File(self, file="samples/function.decaf"):
        '''
        Open the file and read the code from there.
        '''
        self.file = file
        f = open(file)
        s= f.read()
        f.close()
        return s

    def _build_lexer(self):
        '''
        Build the lexer and return a lexer instance.
        '''
        clex = CL()
        clex.build()
        return clex.lexer

    def _build_parser(self):
        '''
        Build the parser.
        '''
        return yacc.yacc(module=DecafParser(), write_tables = 0, start ='program')

    def _type_check(self, parse_tree):
        '''
        Do Type checking
        '''
        # first do symbol table generations
        symbol_table, new_ast = SymbolTableCreator().visit_Program(parse_tree)
#        print('Root Symbol Table %s- \n' % symbol_table)
#        symbol_table.print_symbols()
        
        # do the type checking using the symbol table generated above
        # return the new ast formed after type checking
        return TypeChecker().visit_Program(new_ast,symbol_table)

    def _flow_check(self, parse_tree):
        '''
        Do Flow Checking
        '''
        FlowAnalyser().visit_Program(parse_tree)

    def compile(self):
        '''
        This function is responsible for compiling the source code.
        It creates a parse tree and generates a graph. It then applies 
        semantic analysis one it. After symantic analysis it builds the 
        llvm IR.
        '''
        parse_tree = self.parser.parse(self.code, lexer=self.lexer)
#        self._visualize(parse_tree)

        # Do type checking
        # Get new ast in which types of all nodes are set
        new_ast = self._type_check(parse_tree)

        print("Flow Checking\n")
        # Do Flow Checking
        self._flow_check(parse_tree)

        mod = llvm_core.Module.new(ntpath.basename(self.file).split(".")[0])
        scope = Scope()
        bundle = CodegenBundle(mod,None, scope)
        try:
            for node in parse_tree:
                node.codegen(bundle)
        except:
            print mod
            raise
        print mod
        outfile = "out.ll"
        f = open(outfile,"w")
        f.write(str(mod))
        f.close()
        
    def _create_graph(self, node, graph):
        '''
        
        '''
        if not isinstance(node, Node):
            raise ValueError("A node must be supplied to create_graph, % suppliedinstead" % (type(node),))
        neighbors = []
        for elem in dir(node):
            el = getattr(node,elem)
            if isinstance(el,list):
                for sub_el in el:
                    if isinstance(sub_el,Node):
                        neighbors.append(sub_el)
            if isinstance(el,Node):
                neighbors.append(el)
        for nb in neighbors:
            edge= pydot.Edge(str(node),str(nb))
            graph.add_edge(edge)
            self._create_graph(nb,graph)

    def _visualize(self, root_node_list):
        '''
        This functions is called to create the tree and store it in a png file.
        '''
        graph = pydot.Dot(graph_type='graph')
        # Recursively traverse the tree and create the graph
        for node in root_node_list:
            self._create_graph(node, graph)
        graph.write_png('parse_tree.png')


if __name__ == '__main__':
    # Get the file name as agrument.
    if len(sys.argv) == 2:
        file = sys.argv[1]
    else:
        file = None

    dc = DecafCompiler(file)


