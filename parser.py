#a!/usr/bin/env python

import sys
from lexers import CaffeineLexer as CL
import ply.yacc as yacc
from decafast import *
import typechecker

class Parser(object):
	precedence = (
    ('left', 'COMMA'),
    ('right', 'PRINT'),
    ('left', 'EQUALS'),
    ('left', 'BOOLEAN_OR'),
    ('left', 'BOOLEAN_AND'),
    ('nonassoc', 'IS_EQUAL', 'IS_NOT_EQUAL'),
    ('nonassoc', 'IS_SMALLER', 'IS_SMALLER_OR_EQUAL', 'IS_GREATER', 'IS_GREATER_OR_EQUAL'),
    ('left', 'PLUS', 'MINUS', 'DOT'),
    ('left', 'MUL', 'DIV', 'MOD'),
    ('right', 'BOOLEAN_NOT'),
    ('right', 'LBRACKET'),
    ('nonassoc', 'NEW'),
    ('right', 'INCREMENT', 'DECREMENT'),
    #('left', 'ELSE'),
	)

	def p_program(self, p):
		'''program 	:	declarations'''
		p[0] = p[1]

	def p_declarations(self, p):
		'''declarations : declarations declaration
						| declaration'''
		if len(p) == 3:
			p[0] = p[1] + [p[2]]
		else:
			p[0] = [p[1]]
		
	def p_declaration(self, p):
		'''declaration 	:	variable_declaration
						|	function_declaration
						|	class_declaration
						|	interface_declaration'''
		p[0] = p[1]

	def p_variable_declaration(self, p):
		'''variable_declaration  :	variable SEMICOLON'''
		p[0] = p[1]
		
	def p_variable(self, p):
		'''variable	:	type name'''
		if p[1].is_array():
			p[0] = VariableDecl(p[1], ArrayNameExpr(p[2].value))
		else:
			p[0] = VariableDecl(p[1], p[2])

	def p_type(self, p):
		'''type 	: 	basic_type
		 			|	basic_type LBRACKET LNUMBER RBRACKET'''
		if len(p) == 2:
			p[0] = p[1]
		else:
			p[0] = Type(p[1].type, p[3])


	def p_basic_type(self, p):
		'''basic_type 	: 	INT
					 	|	DOUBLE
					 	|	BOOL
					 	|	STRING
				 		|  	name'''
		p[0] = Type(p[1], 0)


	def p_function_declaration(self, p):
		'''function_declaration 	:	type name LPAREN formal_parameters RPAREN statement_block
									|	VOID name LPAREN formal_parameters RPAREN statement_block'''
		p[0] = FunctionDecl(p[1], p[2], p[4], p[6])

	def p_formal_parameters(self, p):
		'''formal_parameters	:  formal_parameters COMMA variable
								|  variable
								|  empty'''
		if len(p) == 4:
			p[0] = p[1] + [p[3]]
		else:
			p[0] = [p[1]]

	def p_class_declaration(self, p):
		'''class_declaration	:  CLASS name extends_type implements_type LBRACE fields RBRACE'''
		p[0] = ClassDecl(p[2], p[3], p[4], p[6])

	def p_fields(self, p):
		'''fields 	: 	fields field
					|	empty'''
		if len(p) == 2:
			p[0] = []
		else:
			p[0] = p[1] + [p[2]]


	def p_field(self, p):
		'''field 	:	variable_declaration
					|	function_declaration'''
		p[0] = p[1]

	def p_extends_type(self, p):
		'''extends_type  :  EXTENDS name
						 |  empty'''
		if len(p) == 3:
			p[0] = p[2]
		else:
			p[0] = []

	def p_implements_type(self, p):
		'''implements_type  :  IMPLEMENTS names
							|  empty'''
		if len(p) == 3:
			p[0] = p[2]
		else:
			p[0] = []

	def p_names(self, p):
		'''names 	:	names COMMA name
					|	name'''
		if len(p) == 4:
			p[0] = p[1] + [p[3]]
		else:
			p[0] = [p[1]]

	def p_name(self, p):
		'''name 	:	NAME'''
		p[0] = NameExp(p[1])

	def p_interface_declaration(self, p):
		'''interface_declaration 	:	INTERFACE name LBRACE prototypes RBRACE'''
		p[0] = InterfaceDecl(p[2], p[4])


	def p_prototypes(self, p):
		'''prototypes 	:	prototypes prototype
						|	empty'''
		if len(p) == 2:
			p[0] = []
		else:
			p[0] = p[1] + [p[2]]

	def p_prototype(self, p):
		'''prototype 	:	type name LPAREN formal_parameters RPAREN SEMICOLON
						|	VOID name LPAREN formal_parameters RPAREN SEMICOLON'''
		p[0] = PrototypeDecl(p[1], p[2], p[4])

	def p_statement_block(self, p):
		'''statement_block	: 	LBRACE variable_declarations statements RBRACE
							|	LBRACE variable_declarations RBRACE
							|	LBRACE statements RBRACE
							|	LBRACE RBRACE'''
		if len(p) == 3:
			p[0] = []
		elif len(p) == 4:
			p[0] = p[2]
		else:
			p[0] = p[2] + p[3]

	def p_statements(self, p):
		'''statements 	:	statements statement
						|	statement'''
		if len(p) == 3:
			p[0] = p[1] + [p[2]]
		else:
			p[0] = [p[1]]

	def p_variable_declarations(self, p):
		'''variable_declarations 	: 	variable_declarations variable_declaration
									|	variable_declaration'''
		if len(p) == 3:
			p[0] = p[1] + [p[2]]
		else:
			p[0] = [p[1]]

	def p_expressions(self, p):
		'''expressions 	: 	expression
						|	empty'''
		p[0] = p[1]

	def p_statement(self, p):
		'''statement 	:	expressions SEMICOLON
						|	if_statement
						|	while_statement
						|	for_statement
						|	break_statement
						|	return_statement
						|	print_statement
						|	statement_block'''
		p[0] = p[1]

	def p_else_statement(self, p):
		'''else_statement 	:	ELSE statement
							|	empty'''
		if len(p) == 3:
			if 'list' in str(type(p[2])):
				p[0] = p[2]
			else:
				p[0] = [p[2]]
		else:
			p[0] = []

	def p_if_statement(self, p):
		'''if_statement 	:	IF LPAREN expression RPAREN statement else_statement'''
		if('list' in str(type(p[5]))):
			p[0] = IfStmt(p[3], p[5], p[6])
		else:
			p[0] = IfStmt(p[3], [p[5]], p[6])

	def p_while_statement(self, p):
		'''while_statement 	:	WHILE LPAREN expression RPAREN statement'''
		if('list' in str(type(p[5]))):
			p[0] = WhileStmt(p[3], p[5])
		else:
			p[0] = WhileStmt(p[3], [p[5]])			

	def p_for_statement(self, p):
		'''for_statement 	:	FOR LPAREN expressions SEMICOLON expression SEMICOLON expressions RPAREN statement'''
		if('list' in str(type(p[9]))):
			p[0] = ForStmt(p[3], p[5], p[7], p[9])
		else:
			p[0] = ForStmt(p[3], p[5], p[7], [p[9]])

	def p_return_statement(self, p):
		'''return_statement 	:	RETURN expressions SEMICOLON'''
		p[0] = ReturnStmt(p[2])

	def p_break_statement(self, p):
		'''break_statement 	:	BREAK SEMICOLON'''
		p[0] = BreakStmt()

	def p_print_statement(self, p):
		'''print_statement 	:	PRINT LPAREN print_expression RPAREN SEMICOLON'''
		p[0] = PrintStmt(p[3])

	def p_print_expression(self, p):
		'''print_expression 	:	print_expression COMMA expression
								|	expression'''
		if len(p) == 2:
			p[0] = [p[1]]
		else:
			p[0] = p[1] + [p[3]]

	def p_expression(self, p):
		'''expression 	:	increment
						|	decrement 
						|	literal
						|	lvalue
						|	this
						|	call
						|	new_name
						| 	new_array
						|	read_integer
						|	read_line
						|	arithmetic_expression
						|	conditional_expression
						| 	unary_expression
						| 	assignment_expression
						|	LPAREN expression RPAREN'''
		if len(p) == 2:
			p[0] = p[1]
		else:
			p[0] = p[2]

	def p_unary_expression(self, p):
		'''unary_expression 	: 	MINUS expression
								|	BOOLEAN_NOT expression'''
		p[0] = UnaryExpr(p[1], p[2])

	def p_assignment_expression(self, p):
		'''assignment_expression 	: 	lvalue EQUALS expression'''
		p[0] = AssignmentStmt(p[1], p[3])
	
	def p_read_integer(self, p):
		'''read_integer 	: 	READINTEGER LPAREN RPAREN'''
		p[0] = ReadIntegerStmt()

	def p_read_line(self, p):
		'''read_line 	: 	READLINE LPAREN RPAREN'''
		p[0] = ReadLineStmt()

	def p_decrement(self, p):
		'''decrement 	: 	lvalue DECREMENT'''
		p[0] = DecrementExpr(p[1])

	def p_increment(self, p):
		'''increment 	: 	lvalue INCREMENT'''
		p[0] = IncrementExpr(p[1])

	def p_new_name(self, p):
		'''new_name 	: 	NEW name'''
		p[0] = NewStmt(p[2])

	def p_new_array(self, p):
		'''new_array 	: 	NEWARRAY LPAREN expression COMMA type RPAREN'''
		p[0] = NewArrayStmt(p[3], p[5])

	def p_arithmetic_expression(self, p):
		'''arithmetic_expression 	: 	expression PLUS expression
									|	expression MINUS expression
									|	expression MUL expression
									|	expression DIV expression
									|	expression MOD expression
									|	expression BOOLEAN_AND expression
									|	expression BOOLEAN_OR expression'''
		p[0] = ArithmeticExpr(p[2], p[1], p[3])


	def p_conditional_expression(self, p):
		'''conditional_expression 	:	expression IS_SMALLER expression
									|	expression IS_GREATER expression
									|	expression IS_SMALLER_OR_EQUAL expression
									|	expression IS_GREATER_OR_EQUAL expression
									|	expression IS_EQUAL expression
									|	expression IS_NOT_EQUAL expression'''
		p[0] = ConditionalExpr(p[2], p[1], p[3])

	def p_this(self, p):
		'''this 	: 	THIS'''
		p[0] = p[1]

	def p_lvalue(self, p):
		'''lvalue 	:	name
					|	array_name
					|	expression DOT name
					|	expression LBRACKET expression RBRACKET'''
		if len(p) == 2:
			p[0] = p[1]

	def p_array_name(self, p):
		'''array_name 	: 	name LBRACKET expression RBRACKET'''
		p[0] = ArrayNameExpr(p[1], p[3])

	def p_call(self, p):
		'''call 	:	name LPAREN actual_parameters RPAREN
					|	expression DOT name LPAREN actual_parameters RPAREN'''
		if len(p) == 5:
			p[0] = CallExpr(p[1], p[3])
		else:
			p[0] = CallExpr(p[3], p[5], p[1])

	def p_actual_parameters(self, p):
		'''actual_parameters 	: 	print_expression
								|	empty'''
		p[0] = p[1]


	def p_literal(self, p):
		'''literal 	:	dnumber
	                |	lnumber
	                |	string_literal
	                |	true
	                |	false
	                |	null'''
		p[0] = p[1]
        

	def p_dnumber(self,p):
		'''dnumber	:	DNUMBER'''
		p[0] = Literal(p[1], "DNUMBER")

	def p_lnumber(self, p):
		'''lnumber	:	LNUMBER'''
		p[0] = Literal(p[1], "LNUMBER")

	def p_string_literal(self, p):
		'''string_literal	:	STRING_LITERAL'''
		p[0] = Literal(p[1], "string_literal")
	
	def p_true(self, p):
		'''true	:	TRUE'''
		p[0] = Literal(p[1], "true")
	
	def p_false(self, p):
		'''false	:	FALSE'''
		p[0] = Literal(p[1], "false")
	
	def p_null(self, p):
		'''null	:	NULL'''
		p[0] = Literal(p[1], "null")

# used to apply symbol table visitor, type checker visitor etc
def apply_phase(ast,visitor,additional=None):
        """Applies a visitor to the abstract syntax tree."""
        if additional is None:
            return visitor.visit_Program(ast)
        else:
            return visitor.visit_Program(ast,additional)

class DecafParser(Parser):
    # Get the token map
    tokens = CL.tokens
    
    # Error rule for syntax errors
    def p_error(self, t):
        if t:
            raise SyntaxError('invalid syntax', (None, t.lineno, None, t.value))
        else:
            raise SyntaxError('unexpected EOF while parsing', (None, None, None, None))

    def p_empty(self, p):
        '''empty :'''
        p[0] = None
     
import pydot

def create_graph(node, graph):
    if not isinstance(node, Node):
        raise ValueError("A node must be supplied to create_graph, % supplied instead"%(type(node),))
    neighbors = []
    for elem in dir(node):
	el = getattr(node,elem)
	if isinstance(el,list):
            for sub_el in el:
                if isinstance(sub_el,Node):
                    neighbors.append(sub_el)
        if isinstance(el,Node):
            neighbors.append(el)

    for nb in neighbors:
        edge= pydot.Edge(str(node),str(nb))
        graph.add_edge(edge)
        create_graph(nb,graph)


def visualise(root_node_list):
    '''
        Helper method to visualise a parse tree. the only argument needed is a list of nodes
        at the root of the module
    '''
    graph = pydot.Dot(graph_type='graph')
    # Recursively traverse the tree and create the graph
    for node in root_node_list:
        create_graph(node, graph)
    graph.write_png('parse_tree.png')

class ScopeDict(dict):
    '''
        Special dictionary just for chaining scopes
    '''
    def __init__(self, *args):
        super(ScopeDict,self).__init__(self,*args)
        self.parent = None

class Scope:
    '''
        Implements a nested heirarchy of scopes.
        the get and set methods act just like a dictionary.
    '''
    def __init__(self):
        self.top = ScopeDict()
        self.top.parent = None
        setattr(self.top,"parent", None)

    def push_scope(self):
        tmp = ScopeDict()
        tmp.parent = self.top
        self.top = tmp

    def pop_scope(self):
        self.top = self.top.parent

    def get(self,key):
        '''
            traverses up the scopes to find the value associated with a
            name
        '''
        current = self.top
        while not current is None:
            if current.has_key(key):
                return current[key]
            current = current.parent
        raise KeyError("%s is not defined in scope."%(key,))

    def has_name(self,key):
        '''
            Does the key exist in the entire nested scope.
        '''
        current = self.top
        while not current is None:
            if current.has_key(key):
                return True
            current = current.parent
        return False
           
    def exists_in_current_scope(self, key):
        return self.top.has_key(key)

    def set(self,key, value):
        ''' 
            sets value for a particular key in the current scope.
        '''
        self.top[key] = value

# Cant import everything from the module. Specially class Type is defined in both our code and the llvmpy code.
import llvm
from llvm import core as llvm_core

class CodegenBundle:
    def __init__(self, mod, buildr, scope):
        self.module = mod
        self.builder = buildr
        self.scope = scope
        self.constants = {}
        self.return_defined = True
        self.loop_blks = {}

if __name__ == '__main__':
    import readline
    import pprint
    s = ''
    clex = CL()
    clex.build()
    #clex.test()
    lexer = clex.lexer
    # Build the grammar
    parser = yacc.yacc(module=DecafParser(), write_tables = 0, start = 'program')

    s = sys.stdin.read()
    # f = open("samples/functions.decaf")
 #   s= f.read()
    result = parser.parse(s, lexer=lexer, debug = 1)
    #f.close()
    #print(result[0].body[1])

    symbol_table, new_ast = apply_phase(result, typechecker.SymbolTableCreator())
    typ_ck_ast = apply_phase(new_ast, typechecker.TypeChecker(),symbol_table,)

#    visualise(result)
    #print(result[1].body[0])
 #   import ntpath
 #   mod = llvm_core.Module.new(ntpath.basename(filename).split(".")[0])
 #   scope = Scope()
 #   bundle = CodegenBundle(mod,None, scope)
 #   try:
 #       for node in result:
 #           node.codegen(bundle)
 #   except:
 #       print mod
 #       raise
 #   print mod

 #   outfile = "out.ll"
 #   f = open(outfile,"w")
 #   f.write(str(mod))
 #   f.close()
