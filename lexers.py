#!/usr/bin/env python

# This module defines lexer required for. 

import ply.lex as lex


class CaffeineLexer():
    """
	   CaffeineLexer is the class used as a lexer. 
    """

    def __init__(self):
        pass

    #states = (
		# ('decaf', 'exclusive'),
		# ('quoted', 'exclusive'),
		# ('quotedvar', 'exclusive'),
		# ('varname', 'exclusive'),
		# ('offset', 'exclusive'),
		# ('property', 'exclusive'),
		# ('heredoc', 'exclusive'),
		# ('heredocvar', 'exclusive'),
	#)

    # Reserved word
    reserved={
        'NewArray':'NEWARRAY', 
        'bool': 'BOOL', 
        'break':'BREAK', 
        'class':'CLASS', 
        'else':'ELSE',
        'extends':'EXTENDS', 
        'false':'FALSE', 
        'for':'FOR', 
        'if':'IF', 
        'int':'INT', 
        'New':'NEW', 
        'null':'NULL', 
        'return':'RETURN',
        'string':'STRING', 
        'true':'TRUE', 
        'void':'VOID', 
        'while':'WHILE', 
        'this':'THIS', 
        'ReadInteger':'READINTEGER',
        'ReadLine':'READLINE', 
        'double':'DOUBLE', 
        'interface':'INTERFACE', 
        'implements':'IMPLEMENTS', 
        'Print':'PRINT'
        }

    # Not used by parser
    unparsed = [
        # Invisible characters
        'WHITESPACE',
        # Comments
        'COMMENT'
    ]

    tokens = list(reserved.values())+ unparsed + [
        'NAME',
        # Operators
        'PLUS', 'MINUS', 'MUL', 'DIV', 'MOD',
        'BOOLEAN_AND', 'BOOLEAN_OR', 'BOOLEAN_NOT',
        'IS_SMALLER', 'IS_GREATER', 'IS_SMALLER_OR_EQUAL', 'IS_GREATER_OR_EQUAL',
        'IS_EQUAL', 'IS_NOT_EQUAL', 'INCREMENT', 'DECREMENT',

        # Assignment operators
        'EQUALS', 

        # Delimiters
        'LPAREN', 'RPAREN', 'LBRACKET', 'RBRACKET', 'LBRACE', 'RBRACE', 
        'COMMA', 'DOT', 'SEMICOLON', 

        # Identifiers and reserved words
        'VARIABLE',
        'LNUMBER', 'DNUMBER',
        'STRING_LITERAL',
    ]

    def t_NAME(self, t):
        r'[a-zA-Z_][a-zA-Z_0-9]*'
        t.type = self.reserved.get(t.value,'NAME')    # Check for reserved words
        return t


    # Newlines
    '''def t_WHITESPACE(self, t):
        r'[ \t\r\n]+'
        t.lexer.lineno += t.value.count("\n")
        return t'''

    # Operators
    t_PLUS                = r'\+'
    t_MINUS               = r'-'
    t_MUL                 = r'\*'
    t_DIV                 = r'/'
    t_MOD                 = r'%'
    t_BOOLEAN_AND         = r'&&'
    t_BOOLEAN_OR          = r'\|\|'
    t_BOOLEAN_NOT         = r'!'
    t_IS_SMALLER          = r'<'
    t_IS_GREATER          = r'>'
    t_IS_SMALLER_OR_EQUAL = r'<='
    t_IS_GREATER_OR_EQUAL = r'>='
    t_IS_EQUAL            = r'=='
    t_IS_NOT_EQUAL        = r'(!=(?!=))|(<>)'
    t_INCREMENT           = r'\+\+'
    t_DECREMENT           = r'--'
    
    # Assignment operators
    t_EQUALS               = r'='

    # Delimeters
    t_LPAREN               = r'\('
    t_RPAREN               = r'\)'
    t_COMMA                = r','
    t_SEMICOLON            = r';'
    t_DOT                  = r'\.'
    t_ignore               = ' \t\f\r'

    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    def t_newline2(self, t):
        r'(\r\n)+'
        t.lexer.lineno += len(t.value) / 2

    def t_LBRACKET(self, t):
        r'\['
        #t.lexer.push_state('decaf')
        return t

    def t_RBRACKET(self, t):
        r'\]'
        #t.lexer.pop_state()
        return t

    def t_LBRACE(self, t):
        r'\{'
        #t.lexer.push_state('decaf')
        return t

    def t_RBRACE(self, t):
        r'\}'
        #t.lexer.pop_state()
        return t

    # Comments
    def t_COMMENT(self, t):
        r'/\*(.|\n)*?\*/ | //([^?%\n]|[?%](?!>))*\n?'
        t.lexer.lineno += t.value.count("\n")

    # Floating literal
    def t_DNUMBER(self, t):
        r'(\d*\.\d+|\d+\.\d*)([Ee][+-]?\d+)? | (\d+[Ee][+-]?\d+)'
        return t

    # Integer literal
    def t_LNUMBER(self, t):
        r'(0x[0-9A-Fa-f]+)|\d+'
        return t

    # String literal
    def t_STRING_LITERAL(self, t):
        r'"(\\[^\n]|[^"\n])*"'
        t.lexer.lineno += t.value.count("\n")
        return t

    def t_ANY_error(self, t):
        raise SyntaxError('illegal character', (None, t.lineno, None, t.value))

    def peek(self, lexer):
        try:
            return lexer.lexdata[lexer.lexpos]
        except IndexError:
            return ''

    # Build the lexer
    def build(self,**kwargs):
        self.lexer = lex.lex(module=self, **kwargs)

    # Test. Print the output
    def test(self):
        lex.runmain()


if __name__ == '__main__':
    lexer = CaffeineLexer()
    lexer.build()
    lexer.test()
