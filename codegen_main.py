from parser import *
if __name__ == '__main__':
    import readline
    import pprint
    s = ''
    clex = CL()
    clex.build()
    #clex.test()
    lexer = clex.lexer
    # Build the grammar
    parser = yacc.yacc(module=DecafParser(), write_tables = 0, start = 'program')

    #s = sys.stdin.read()
    #filename = "samples/functions.decaf"
    filename = "samples/gcd.decaf"
    f = open(filename)
    s= f.read()
    result = parser.parse(s, lexer=lexer, debug = 1)
    #f.close()
    #print(result[0].body[1])

	# symbol table is being created but None is returned, fix this!
    #symbol_table = apply_phase(result, typechecker.SymbolTableCreator())

#    visualise(result)
    #print(result[1].body[0])
    import ntpath
    mod = llvm_core.Module.new(ntpath.basename(filename).split(".")[0])
    scope = Scope()
    bundle = CodegenBundle(mod,None, scope)
    try:
        for node in result:
            node.codegen(bundle)
    except:
        print mod
        raise
    print mod

    outfile = "out.ll"
    f = open(outfile,"w")
    f.write(str(mod))
    f.close()
